## MantistTable Tool

We have 6 implementations of MantisTable developed over time, the latest was rebranded as s-Elbat:

1. https://bitbucket.org/disco_unimib/mantistable-tool/
here there is our first implementation in Meteor/NodeJS.
In this version we faced some scalability issues so it was later reimplemented from scratch.

2. https://bitbucket.org/disco_unimib/mantistable-tool.py
The second version is a more computationally efficient version in python

3. While the 3rd version is the one used during the SemTab 2019 Challenge
https://bitbucket.org/disco_unimib/mantistable-tool-3

4. The 4th version is a new implementation with a new algorithm that was tested on SemTab2020,
It is also known as MantisTable SE and is available 
https://bitbucket.org/disco_unimib/mantistable-4

5. MantisTable V
https://bitbucket.org/disco_unimib/mantistable-v
https://bitbucket.org/disco_unimib/mantistable-v-api

6. s-Elbat (latest implementation)
https://bitbucket.org/disco_unimib/selbat 

## How to run MantisTable:

The first step consists in installing the required dependencies
    `npm install`
(Node 8+ is required with an updated npm version)

We used docker-compose from the beginning, so in order to run any version of MantisTable it's enough to use the command:
    `docker-compose up`

inside the docker-compose.yml file you will find the ports where the service will start
(actually port 8007 but it can be customized)
on line 38 replace "16" in "--autoscale=16,4" with the number of available Cores on your Server.

## Demo:
On [http://zoo.disco.unimib.it](http://zoo.disco.unimib.it) we have the main information about our tools and there is a link to a running demo of the 3rd version [http://149.132.176.50:8092](http://149.132.176.50:8092) 